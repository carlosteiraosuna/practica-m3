import datetime

master_dict = {'datos_clientes': ["id", 'nombre', 'apellidos', 'numero_de_telefono', 'direccion', 'comercial'],
               'datos_comerciales': ["id", 'nombre', 'apellidos', 'numero_de_telefono', 'comision', 'clientes_asociados',
                                     'visitas'], 'datos_visitas': ['fecha', 'hora', 'direccion', 'comercial'
        , 'cliente'], 'datos_campanas': ['comerciales', 'mensaje', 'estado']}

crm_dict = {"clientes": {1: {'nombre': 'Javi', 'apellidos': 'Melon', 'numero_de_telefono': '123', 'direccion': 'none',
             'comercial': 1},
            2: {'nombre': 'Antonio', 'apellidos': 'Clavo', 'numero_de_telefono': '456',
             'direccion': 'None', 'comercial': 1}},
            "comerciales": {1: {'id': 1, 'nombre': 'Pep', 'apellidos': 'Vila', 'numero_de_telefono': '123', 'comision': 0.5,
                                'clientes_asociados': [], 'visitas': {}}}, "campanas": {}}
import datetime as dt

"""
Apartado 2
"""

"""2.1 – Lista todos los comerciales  con el siguiente formato:

[id de comercial] – [Nombre] [Apellido] | num clientes [Número de clientes asignados]
[id de comercial] – [Nombre] [Apellido] | num clientes [Número de clientes asignados]
[id de comercial] – [Nombre] [Apellido] | num clientes [Número de clientes asignados]
"""
for comercial in crm_dict["comerciales"]:
    clientes_asociados = crm_dict["comerciales"][comercial]["clientes_asociados"]
    num_clientes = len(clientes_asociados)
    print(comercial, comerciales[comercial]["nombre"], crm_dict["comerciales"][comercial]["apellidos"],
          "| num clientes: ", num_clientes)

"""2.2 – Pide el id del comercial que se quiera seleccionar y muestra el submenu  de seleccionar comercial.
2.2.1 – Muestra uno a uno los datos del comercial y pregunta si se quieren modificar."""
"""
id_comercial = int(input("Id de comercial: "))
while id_comercial not in crm_dict["comerciales"]:
    id_comercial = int(input("Ese Id de comercial no existe. Introduzca id válido: "))
for etiqueta in master_dict["datos_comerciales"]:
    print(etiqueta + ": ", crm_dict["comerciales"][id_comercial][etiqueta])
    modificar = None
    while modificar not in ["S", "N"]:
        modificar = input("¿Modificar " + etiqueta + "?").upper()
    if modificar == "S":
        if etiqueta not in ["clientes_asociados", "visitas"]:
            print(etiqueta + "actual: " + crm_dict["comerciales"][id_comercial][etiqueta])
            valor_nuevo = input("Nuevo/a" + etiqueta + ": ")"""

"""
2.2.2 – Muestra las próximas visitas del comercial, el sistema debe dar la opción de ver las visitas de mañana,
las de la próxima semana y las del próximo mes. Las visitas se muestran ordenadas de mas cercana a más lejana
en el tiempo en el siguiente formato:
[día] [hora] – [nombre cliente] [apellidos cliente] – [dirección cliente]"""
"""
id_comercial = int(input("Id de comercial: "))
while id_comercial not in crm_dict["comerciales"]:
    id_comercial = int(input("Ese Id de comercial no existe. Introduzca id válido: "))
for etiqueta in master_dict["datos_comerciales"]:
    print(etiqueta + ": ", crm_dict["comerciales"][id_comercial][etiqueta])
    modificar = None
    while modificar not in ["S", "N"]:
        modificar = input("¿Modificar" + etiqueta + "?").upper()
    if modificar == "S":
        if etiqueta not in ["clientes_asociados", "visitas"]:
            print(etiqueta + "actual: " + crm_dict["comerciales"][id_comercial][etiqueta])
            valor_nuevo = input("Nuevo/a" + etiqueta + ": ")"""
print(crm_dict["clientes"].keys())
"""
2.2.3 – Pide el día, pide la hora y el id del cliente para asignarle una visita al comercial."""
id_comercial = 1
id_cliente = None
while id_cliente not in crm_dict["clientes"]:
    id_cliente = int(input("Cliente visita: "))
fecha_visita = input("Fecha de la visita: \n(Formato 'ddmmaaaa')")
dia = int(fecha_visita[:2])
mes = int(fecha_visita[2:4])
any = int(fecha_visita[4::])
hora = input("Hora de la visita: ")
visitas = crm_dict["comerciales"][id_comercial]["visitas"]
id_visita = str(id_cliente) + "|" + str(fecha_visita) + "|" + str(hora)
if id_visita not in visitas.keys():
    visitas[id_visita] = {}
    visitas[id_visita]["fecha_visita"] = dt.datetime(any, mes, dia)
    visitas[id_visita]["hora"] = hora
    visitas[id_visita]["cliente"] = id_cliente

"""
2.2.4 – Muestra las visitas futuras del comercial, permite seleccionar una y eliminarla"""
hoy = dt.datetime.now()
dt_fecha_visita = visitas[id_visita]["fecha_visita"]
visitas_1_mes = []
visitas_1_semana = []
visitas_manana = []

if hoy + dt.timedelta(days=30) >= dt_fecha_visita:
    visitas_1_mes.append((id_visita, visitas[id_visita]))
if hoy + dt.timedelta(days=7) >= dt_fecha_visita:
    visitas_1_semana.append((id_visita, visitas[id_visita]))
if hoy + dt.timedelta(days=1) >= dt_fecha_visita:
    visitas_manana.append((id_visita, visitas[id_visita]))

print((dt_fecha_visita - hoy).days)
print(hoy)
print(dt_fecha_visita)
print("Mes vista: ", visitas_1_mes)
print("Semana vista: ", visitas_1_semana)
print("Visitas Mañana: ", visitas_manana)


"""
2.3 – Pide uno por uno los datos  necesarios para crear un comercial y los almacena."""

# Creacion de un comercial nuevo

# Recorre todos los campos de comercial
for etiqueta in master_dict["datos_comerciales"]:

    # Diferencia casos en los que el campo debe ser formato 'texto', 'numero' o 'lista', e ignora las visitas
    if etiqueta not in ["clientes_asociados", "visitas", "numero_de_telefono", "comision"]:
        crm_dict["comerciales"][id_comercial][etiqueta] = input("Introduzca un " + etiqueta).capitalize()
        id_comercial = max(crm_dict["ID_comerciales"].keys()) + 1
        crm_dict["comerciales"][id_comercial] = {}
        crm_dict["comerciales"][id_comercial]["id"] = id_comercial
    elif etiqueta == "numero_de_telefono":
        is_num = False
        while is_num is False:
            try:
                crm_dict["comerciales"][id_comercial][etiqueta] = int(input("Introduzca un " + etiqueta))
                is_num = True
            except ValueError:
                print("El valor introducido para este campo debe ser un número")
    elif etiqueta == "comision":
        is_num = False
        is_pct = False
        while is_num is False or is_pct is False:
            try:
                crm_dict["comerciales"][id_comercial][etiqueta] = float(input("Introduzca un porcentaje de " + etiqueta))
                is_num = True
                if 0 <= crm_dict["comerciales"][id_comercial][etiqueta] < 100:
                    is_pct = True
                else:
                    print("Un porcentaje de comision debe estar entre 0 y 100")
            except ValueError:
                print("El valor introducido para este campo debe ser un número")
    elif etiqueta == "clientes_asociados":
        crm_dict["comerciales"][id_comercial]["clientes_asociados"] = []
        agregar_cliente = None
        while agregar_cliente != "N":
            agregar_cliente = input("¿Agregar cliente asociado a este comercial? (S/N)").upper()
            if agregar_cliente == "S":
                cliente_asociado = int(input("ID de cliente asociado: "))
                while cliente_asociado not in crm_dict["clientes"]:
                    print("ID de cliente no válida")
                    cliente_asociado = int(input("ID de cliente asociado: "))
                # Hay que añadir un borrado de cliente en el campo correspondiente del comercial anterior

                crm_dict["clientes"][cliente_asociado]["comercial"] = id_comercial
                crm_dict["comerciales"][id_comercial]["clientes_asociados"].append(cliente_asociado)
print(crm_dict["comerciales"])

for comercial in crm_dict["comerciales"]:
    clientes_asociados = crm_dict["comerciales"][comercial]["clientes_asociados"]

    """for cliente in crm_dict["clientes"]:
        if crm_dict["clientes"][cliente]["comercial"] == comercial:
            clientes_asociados.append(cliente)"""
    num_clientes = len(clientes_asociados)
    print(comercial, crm_dict["comerciales"][comercial]["nombre"], crm_dict["comerciales"][comercial]["apellidos"],
          "| num clientes: ", num_clientes)
"""
2.4 – Elimina el comercial borrando todas las visitas que tuviese asignadas y eliminando cualquier
 vinculo que hubiese entre ese comercial y cualquier cliente.
"""

comercial_borrado = int(input("ID de comercial que se desea borrar: "))
print(comercial_borrado, crm_dict["comerciales"][comercial_borrado]["nombre"], crm_dict["comerciales"][comercial_borrado]["apellidos"],
          "| num clientes: ", len(crm_dict["comerciales"][comercial_borrado]["clientes_asociados"]))
for cliente in crm_dict["clientes"]:
        if crm_dict["clientes"][cliente]["comercial"] == comercial_borrado:
            crm_dict["clientes"][cliente].pop("comercial")
print(crm_dict["clientes"])







