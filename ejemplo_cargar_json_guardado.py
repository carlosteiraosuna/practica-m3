import json
try:
    with open("mi_dicc.txt", "r") as f:
        mi_dicc = json.loads(f.read())
except FileNotFoundError:
    mi_dicc = {}
    print("No hay archivo de guardado. Se ha creado un diccionario vacío.")