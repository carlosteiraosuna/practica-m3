import datetime as dt
# Diccionario con etiquetas que debe tener cada elemento
master_dict = {'datos_clientes': ['nombre', 'apellidos', 'numero_de_telefono', 'direccion', 'comercial'],
               'datos_comerciales': ['nombre', 'apellidos', 'numero_de_telefono', 'comision', 'clientes_asociados',
                                     'visitas'],
               'datos_visitas': ['fecha', 'hora', 'direccion', 'comercial', 'cliente'],
               'datos_campanas': ['nombre', 'comerciales', 'mensaje', 'estado']}

# Diccionario con todos los datos
crm_dict = {"clientes": {1: {'nombre': 'Javi', 'apellidos': 'Melon', 'numero_de_telefono': '123', 'direccion': 'none',
             'comercial': 1},
            2: {'nombre': 'Antonio', 'apellidos': 'Clavo', 'numero_de_telefono': '456',
             'direccion': 'None', 'comercial': 1}},

            "comerciales": {1: {'nombre': 'Pep', 'apellidos': 'Vila', 'numero_de_telefono': '123', 'comision': 0.5,
                                'clientes_asociados': [1, 2], 'visitas': {'15022020|15': {'fecha_visita': dt.datetime(2020, 2, 15, 0, 0), 'hora': 15,
                                                            'direccion': 'Calle Falsa 678', 'cliente': 1},
                                            '25012020|19': {'fecha_visita': dt.datetime(2020, 1, 25, 0, 0), 'hora': 19,
                                                            'direccion': 'Calle Falsa 988', 'cliente': 1},
                                            '31012020|12': {'fecha_visita': dt.datetime(2020, 1, 31, 0, 0), 'hora': 12,
                                                            'direccion': 'Calle Falsa 678', 'cliente': 1},
                                            '15022020|13': {'fecha_visita': dt.datetime(2020, 2, 15, 0, 0), 'hora': 13,
                                                            'direccion': 'Calle Falsa 678', 'cliente': 1}}}},

            "campanas": {1: {'nombre': 'verano', 'comerciales': '1', 'mensaje': '"Hola"', 'estado': 'pendiente'},
                         2: {'nombre': 'invierno', 'comerciales': '2', 'mensaje': '"Adios"', 'estado': 'pendiente'}},

            "ids_clientes": [1, 2],

            "ids_comerciales": [1]}

# Variables para acceder a elementos del diccionario
clientes = crm_dict["clientes"]
comerciales = crm_dict["comerciales"]
campanas = crm_dict["campanas"]

ids_clientes = crm_dict["ids_clientes"]
ids_comerciales = crm_dict["ids_comerciales"]


# INICIO PRACTICA FINAL UF01

Inicio_programa = True

while Inicio_programa == True:
    print("Customer Relationship Management")
    print("1. Menu clientes")
    print("2. Menu comerciales")
    print("3. Menu email marketing")
    print("4. Salir")
    menu_principal = int(input("Indicar numero de menu principal"))
    menu_principal_boo = True
# 1. CLIENTES ----------------------------------------------------------------------------------------------------------
    while menu_principal == 1 and menu_principal_boo == True:
        print("1 Listar clientes")
        print("2 Seleccionar cliente")
        print("3 Añadir cliente")
        print("4 Eliminar cliente")
        print("5 Menu anterior")
        menu_cliente = int(input("Indicar numero de menu cliente"))
        menu_cliente_boo = True
# 1. 1. LISTAR CLIENTES ------------------------------------------------------------------------------------------------
        if menu_cliente == 1:
            print("clientes listados")
# 1. 2. SELECCION CLIENTE ----------------------------------------------------------------------------------------------
        while menu_cliente == 2 and menu_cliente_boo == True:
            print("1 Editar datos del cliente")
            print("2 Asignar comercial")
            print("3 Menu anterior")
            submenu_cliente = int(input("Indicar numero de submenu cliente"))
    # 1. 2. 1. MODIFICAR DATOS CLIENTE ---------------------------------------------------------------------------------
            if submenu_cliente == 1:
                print("datos cliente a editar")
    # 1. 2. 2. ASIGNAR COMERCIAL ---------------------------------------------------------------------------------------
            if submenu_cliente == 2:
                print("Asignaciones de comerciales")
    # 1. 2. 3. VOLVER A MENU CLIENTES ?---------------------------------------------------------------------------------
            if submenu_cliente == 3:
                print("Vuelta a menu")
                menu_cliente_boo = False
# 1. 3. AÑADIR CLIENTE -------------------------------------------------------------------------------------------------
        if menu_cliente == 3:
            print("Cliente a añadir")
# 1. 4. ELIMINAR CLIENTE -----------------------------------------------------------------------------------------------
        if menu_cliente == 4:
            print("Cliente a eliminar")
# 1. 5. VOLVER A MENU PRINCIPAL DESDE CLIENTES -------------------------------------------------------------------------
        if menu_cliente == 5:
            print("Vuelta a menu principal")
            menu_principal_boo = False
# 2 COMERCIALES --------------------------------------------------------------------------------------------------------
    while menu_principal == 2 and menu_principal_boo == True:
        print("1 Listar comerciales")
        print("2 Seleccionar comercial")
        print("3 Añadir comercial")
        print("4 Eliminar comercial")
        print("5 Menu anterior")
        menu_comercial = int(input("Indicar numero de menu comercial"))
        menu_comercial_boo = True
# 2. 1. LISTAR COMERCIALES ---------------------------------------------------------------------------------------------
        if menu_comercial == 1:
            print("-------------------------------------------------------------------------------------------")
            print("Lista de comerciales")
            print("-------------------------------------------------------------------------------------------")
            for comercial in crm_dict["comerciales"]:
                clientes_asociados = comerciales[comercial]["clientes_asociados"]
                num_clientes = len(clientes_asociados)
                print(comercial, comerciales[comercial]["nombre"],
                      comerciales[comercial]["apellidos"],
                      "| num clientes: ", num_clientes)
            print("-------------------------------------------------------------------------------------------")
# 2. 2. SELECCION COMERCIAL --------------------------------------------------------------------------------------------
        while menu_comercial == 2 and menu_comercial_boo == True:
            print("-------------------------------------------------------------------------------------------")
            print("Seleccion de comercial")
            print("-------------------------------------------------------------------------------------------")
            if len(comerciales) != 0:
                seleccion_comercial_ok = False
                while not seleccion_comercial_ok:
                    try:
                        comercial_seleccionado = int(input("ID de comercial a mostrar: "))
                        print("Los datos del comercial seleccionado son: \n", comercial_seleccionado,
                              comerciales[comercial_seleccionado]["nombre"],
                              comerciales[comercial_seleccionado]["apellidos"],
                              "| num clientes: ", len(comerciales[comercial_seleccionado]["clientes_asociados"]))
                        seleccion_comercial_ok = True
                    except ValueError:
                        print("La ID de comercial debe ser un numero entero")
                    except KeyError:
                        print("No existe un comercial con esa ID")
                print("-------------------------------------------------------------------------------------------")
                print("Seleccion de comercial")
                print("-------------------------------------------------------------------------------------------")
                print("1 Editar datos del comercial")
                print("2 Asignar visitas")
                print("3 Mostrar visitas del comercial")
                print("4 Eliminar visita")
                print("5 Menu anterior")
                submenu_comercial = int(input())
                while submenu_comercial not in [1, 2, 3, 4, 5]:
                    print("No existe esa opcion")
# 2. 2. 1. MODIFICAR DATOS COMERCIAL -------------------------------------------------------------------------------
                if submenu_comercial == 1:
                    print("datos comercial a editar")
# 2. 2. 2. ASIGNAR VISITAS AL COMERCIAL-----------------------------------------------------------------------------
                if submenu_comercial == 2:
                    print("Asignaciones de visitas")
# 2. 2. 3.  MOSTRAR VISITAS ----------------------------------------------------------------------------------------
                if submenu_comercial == 3:
                    visitas = comerciales[comercial_seleccionado]["visitas"]
                    hoy = dt.datetime.now()
                    visitas_mostradas = []
                    print("1: Mostrar visitas de mañana.\n"
                          "2: Mostrar visitas en la próxima semana.\n"
                          "3: Mostrar visitas a un mes vista.")
                    mostrar_visitas = None
                    while mostrar_visitas not in [1, 2, 3]:
                        try:
                            mostrar_visitas = int(input())
                        except ValueError:
                            pass
                    if mostrar_visitas == 1:
                        visitas_mostradas = []
                        for id_visita in visitas:
                            fecha_visita = visitas[id_visita]["fecha_visita"]
                            hora = visitas[id_visita]["hora"]
                            if hoy + dt.timedelta(days=1) >= fecha_visita:
                                visitas_mostradas.append((fecha_visita, hora, id_visita))
                    if mostrar_visitas == 2:
                        visitas_mostradas = []
                        for id_visita in visitas:
                            fecha_visita = visitas[id_visita]["fecha_visita"]
                            hora = visitas[id_visita]["hora"]
                            if hoy + dt.timedelta(days=7) >= fecha_visita:
                                visitas_mostradas.append((fecha_visita, hora, id_visita))
                    if mostrar_visitas == 3:
                        visitas_mostradas = []
                        for id_visita in visitas:
                            fecha_visita = visitas[id_visita]["fecha_visita"]
                            hora = visitas[id_visita]["hora"]
                            if hoy + dt.timedelta(days=30) >= fecha_visita:
                                visitas_mostradas.append((fecha_visita, hora, id_visita))

                    for visita in sorted(visitas_mostradas):
                        id_visita = visita[2]
                        print(
                            str(visitas[id_visita]["fecha_visita"].day) + "/" +
                            str(visitas[id_visita]["fecha_visita"].month) + "/" +
                            str(visitas[id_visita]["fecha_visita"].year) + "|" +
                            str(visitas[id_visita]["hora"]) + "h,",
                            "ID Cliente:", str(visitas[id_visita]["cliente"]) + ",",
                            "Direccion de la visita:", str(visitas[id_visita]["direccion"]) + ".")
# 2. 2. 4. ELIMINAR VISITA -----------------------------------------------------------------------------------------
                if submenu_comercial == 4:
                    print("Eliminar visita")
# 2. 2. 5. VOLVER AL MENU DE COMERCIALES ? -------------------------------------------------------------------------
                if submenu_comercial == 5:
                    print("Vuelta a menu")
                    menu_comercial_boo = False

            else:
                print("No existen comerciales. Cree alguno primero.")
                menu_comercial_boo = False
# 2. 3. CREAR COMERCIAL ------------------------------------------------------------------------------------------------
        if menu_comercial == 3:
            print("Comercial a añadir")

# 2. 4. ELIMINAR COMERCIAL ---------------------------------------------------------------------------------------------
        if menu_comercial == 4:
            print("Comercial a eliminar")
            # Establece un bucle para el borrado de comerciales
            borrar_comercial = True
            while borrar_comercial:
                # Pide una ID de comercial para borrarlo, muestra datos por pantalla y pide confirmacion
                if len(comerciales) != 0:
                    seleccion_comercial_ok = False
                    while not seleccion_comercial_ok:
                        try:
                            comercial_borrado = int(input("ID de comercial que se desea borrar: "))
                            print("Los datos del comercial que desea borrar son: \n", comercial_borrado,
                                  comerciales[comercial_borrado]["nombre"],
                                  comerciales[comercial_borrado]["apellidos"],
                                  "| num clientes: ", len(comerciales[comercial_borrado]["clientes_asociados"]))
                            seleccion_comercial_ok = True
                        except ValueError:
                            print("La ID de comercial debe ser un numero entero.")
                        except KeyError:
                            print("No existe un comercial con esa ID.")
                    confirmar_borrado = False
                    while confirmar_borrado not in ["S", "N"]:
                        confirmar_borrado = input(
                            "¿Seguro que quiere borrar este comercial? Este borrado es permanente"
                            " y su ficha no podrá ser recuperada. (S/N)").upper()
                    if confirmar_borrado == "S":
                        # Borrado del comercial
                        for cliente in comerciales[comercial_borrado]["clientes_asociados"]:
                            clientes[int(cliente)]["comercial"] = None
                        comerciales.pop(comercial_borrado)
                    borrar_comercial = False
                    seguir_borrando = False
                    while seguir_borrando not in ["S", "N"] and len(comerciales) != 0:
                        seguir_borrando = input("Borrar otro comercial?").upper()
                    if seguir_borrando == "S":
                        borrar_comercial = True

                else:
                    print("No existen fichas de comerciales. Cree alguna primero.")
# 2. 5. MENU PRINCIPAL VOLVER DESDE COMERCIALES ------------------------------------------------------------------------
        if menu_comercial == 5:
            print("Vuelta a menu principal")
            menu_principal_boo = False
# 3. CAMPAÑAS ----------------------------------------------------------------------------------------------------------
    while menu_principal == 3 and menu_principal_boo == True:
        print("1 Preparar campaña")
        print("2 Enviar campaña")
        print("3 Menu anterior")
        menu_marketing = int(input("Indicar numero de menu Marketing"))
        menu_marketing_boo = True
# 3. 1. CREACION CAMPAÑA -----------------------------------------------------------------------------------------------
        if menu_marketing == 1:
            print("Campaña preparada")
# 3. 2. ENVIAR CAMPAÑA -------------------------------------------------------------------------------------------------
        if menu_marketing == 2:
            print("Campaña enviada")
# 3. 3. VUELVE A MENU PRINCIPAL DESDE MARKETING ------------------------------------------------------------------------
        if menu_marketing == 3:
            print("Vuelta a menu de Marketing")
            menu_principal_boo = False
# 4. SALIR DEL PROGRAMA ------------------------------------------------------------------------------------------------
    if menu_principal == 4:
        print("Programa finalizado")
        Inicio_programa = False


